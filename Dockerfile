FROM hepdock/root:6.34.02-ubuntu24.04

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# APT Package Manager + LibTorch
COPY Dockerfile.packages packages
RUN apt-get update -qq \
    && ln -sf /usr/share/zoneinfo/UTC /etc/localtime \
    && apt-get -y install $(awk -F '#' '{print $1}' packages) \
    && apt-get autoremove -y && apt-get clean -y \
    && rm -rf /var/cache/apt/archives/* && rm -rf /var/lib/apt/lists/*

# Conda Environment Manager
ENV PATH=/opt/conda/bin:$PATH
COPY requirements.txt /tmp/requirements.txt
RUN SHFILE="https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh" \
    && wget -O conda.sh $SHFILE && sh ./conda.sh -b -p "/opt/conda" && rm conda.sh

# ROOT+ and LibTorch Installation
ENV Torch_VERSION="2.5.1"
ENV CLING_MODULEMAP_FILES="/usr/local/lib/RIOPlus.modulemap:$CLING_MODULEMAP_FILES"
RUN git clone --recursive https://git.ligo.org/kagra/libraries-addons/root/root-plus.git /opt/root-plus \
    && mkdir -p /opt/root-plus/build && cd /opt/root-plus/build \
    && ../cmake/download_libtorch ${Torch_VERSION} && rsync -avu libtorch/* /usr/local \
    && cmake .. && make -j$(nproc) && make install \
    && rm -rf /opt/root-plus

# Install RCParser library
ENV CLING_MODULEMAP_FILES="/usr/local/lib/RCParser.modulemap:$CLING_MODULEMAP_FILES"
RUN git clone --recursive https://git.ligo.org/kagra/libraries-addons/root/cparser.git /opt/cparser \
    && cd /opt/cparser && mkdir -p var/build && cd var/build && cmake ../.. \
    && cd /opt/cparser/var/build && make -j$(nproc) && make -j$(nproc) install \
    && rm -rf /opt/cparser

# Install RDKafka library
RUN git clone --recursive https://github.com/confluentinc/librdkafka /opt/rdkafka \
    && cd /opt/rdkafka && ./configure && make -j$(nproc) && make -j$(nproc) install \
    && rm -rf /opt/rdkafka

# Install KAFKA server: @TODO to be replaced by a docker-compose entry
# RUN wget https://dlcdn.apache.org/kafka/3.7.1/kafka-3.7.1-src.tgz \
#     && tar -xzf kafka-3.7.1-src.tgz && mv kafka-3.7.1-src kafka \
#     && cd /opt/kafka && ./gradlew jar -PscalaVersion=2.13.12 \
#     && /opt/kafka/bin/kafka-storage.sh format -t $(/opt/kafka/bin/kafka-storage.sh random-uuid) -c /opt/kafka/config/kraft/server.properties \
#     && rm -rf /opt/kafka-3.7.1-src.tgz

# EXPOSE 9092

WORKDIR /root
COPY .condarc .
COPY .bashrc .
COPY .env .

CMD ["bash"]