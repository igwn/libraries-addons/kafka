/**
 **********************************************
 *
 * \file TKafka.cc
 * \brief Source code of the TKafka class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <TKafka.h>
#include <RKafka/TKafkaImpl.h>

void TKafka::Impl::_dr_msg_cb(rd_kafka_t *rk, const rd_kafka_message_t *rkmessage, void *opaque) {

	UNUSED(rk);
	UNUSED(opaque);

    if (rkmessage->err) TPrint::Error(__METHOD_NAME__, "Message delivery failed: %s", rd_kafka_err2str(rkmessage->err));
    else TPrint::Message(__METHOD_NAME__, "Message delivered (%zd bytes, partition %" PRId32 ")", rkmessage->len, rkmessage->partition);

    TKafka::bCallback = true;

    /* The rkmessage is destroyed automatically by librdkafka */
}

std::vector<TKafka::Broker> TKafka::Impl::ParseBroker(std::vector<TString> hostport)
{
        std::vector<TKafka::Broker> brokers;
        for(int i = 0, N = hostport.size(); i < N; i++) {
                brokers.push_back(ParseBroker(hostport[i]));
        }

        return brokers;
}

TKafka::Broker TKafka::Impl::ParseBroker(TString hostport)
{
    TObjArray *array = hostport.Tokenize(":");
    TString host = (array->GetEntries() > 0) ? ((TObjString *)(array->At(0)))->String() : TString(TKafka::DEFAULT_HOST);
    int port = (array->GetEntries() > 1) ? ((TObjString *)(array->At(1)))->String().Atoi() : TKafka::DEFAULT_PORT;

    TKafka::Broker b;
                   b.host = host;
                   b.port = port;

    return b;
}

TKafka::Record TKafka::Impl::ParseRecord(rd_kafka_message_t *rkm)
{
        time_t epoch;
        time(&epoch);

        TString key = ROOT::IOPlus::Helpers::IsPrintable((const char*) rkm->key, rkm->key_len) ? TString((const char*)rkm->key, rkm->key_len) : "";
        TString msg;

        TKafka::Record r;
                r.topic     = rd_kafka_topic_name(rkm->rkt);
                r.offset    = rkm->offset;
                r.partition = rkm->partition;
                r.key       = key;
                r.msg       = msg;
                r.epoch     = epoch;

        return r;
}

TKafka::Impl::~Impl()
{
    /* Destroy the config */
    if(conf != NULL) rd_kafka_conf_destroy(conf);

    /* Close the consumer: commit final offsets and leave the group. */
    if(rk != NULL) rd_kafka_consumer_close(rk);

    /* Destroy the consumer */
    if(rk != NULL) rd_kafka_destroy(rk);
}