/**
 * *********************************************
 *
 * \file TKafka.h
 * \brief Header of the TKafka class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TKafka
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date May 19th, 2022
 *
 * *********************************************
 */

#ifndef TKafkaImpl_H
#define TKafkaImpl_H

#include <TKafka.h>

#include <TCli.h>
#include <TPrint.h>

#include <inttypes.h>
// #include <openssl/bio.h>
// #include <openssl/evp.h>
// #include <openssl/rand.h>
#include "librdkafka/rdkafka.h"
// #include <time.h>
// #include <signal.h>
// #include <string.h>
// #include <ctype.h>

#include <unistd.h> 
#include <sstream> 

class TKafka::Impl
{
    friend class TKafka;
        
    protected:
        static TKafka::Broker ParseBroker(TString hostport);
        static std::vector<TKafka::Broker> ParseBroker(std::vector<TString> hostport);

        static TKafka::Record ParseRecord(rd_kafka_message_t *);

        static void _dr_msg_cb(rd_kafka_t *rk, const rd_kafka_message_t *rkmessage, void *opaque);

        rd_kafka_t *rk = NULL;          /* Consumer instance handle */
        rd_kafka_t *GetInstance() { return rk; };
        void SetInstance(rd_kafka_t *rk) { this->rk = rk; };

        rd_kafka_conf_t *conf = NULL;   /* Temporary configuration object */
        rd_kafka_conf_t *GetConfiguration() { return conf; };
        void SetConfiguration(rd_kafka_conf_t *conf) { this->conf = conf; }

        rd_kafka_topic_partition_list_t *subscription = NULL; /* Subscribed topics */
        rd_kafka_topic_partition_list_t *GetSubscription() { return subscription; };
        void SetSubscription(rd_kafka_topic_partition_list_t *subscription) { this->subscription = subscription; }
        
    protected: 
        Impl() { }

    public:
        ~Impl();
};

#endif