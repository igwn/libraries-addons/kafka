# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    find_package(TBB REQUIRED)
endif()

# Load using standard package finder
find_package_standard(
    NAMES RIOPlus
    HEADERS "Rioplus.h"
    PATHS ${ROOTPLUS} ${ROOTPLUS_DIR} $ENV{ROOTPLUS} $ENV{ROOTPLUS_DIR}
)

#
# Prefer to use default FindTorch, if available
get_filename_component(FILE_NAME ${CMAKE_CURRENT_LIST_FILE} NAME_WE)
if(${FILE_NAME} MATCHES "^Find(.+)$")

    set(FINDER ${CMAKE_MATCH_1})
    if(${FINDER}_FOUND)
            set(${FINDER}_LIBRARIES "${${FINDER}_LIBRARIES}" "${TBB_LIBRARIES}")
            set(${FINDER}_INCLUDE_DIRS "${${FINDER}_INCLUDE_DIRS}" "${TBB_INCLUDE_DIRS}")
            set(${FINDER}_DIR "${${FINDER}_DIR}")
    endif()
endif()
