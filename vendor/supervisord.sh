#!/bin/bash

# Start Supervisor server
/usr/bin/supervisord -c /etc/supervisor/supervisord.conf

# Start an interactive bash shell
/bin/bash
