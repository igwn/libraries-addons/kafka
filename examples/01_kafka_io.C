#include <Riostream.h>
#include <TCli.h>

#include <TKafka.h>

int main(int argc, char const *argv[]) {

    TString hostname = "localhost";
    int port = 9092;
    
    TString topics = "quickstart-events";

    TKafka kafka(hostname, port, topics);
           kafka.Connect(TKafka::Mode::Consumer);

    std::vector<TKafka::Record> msg = kafka.Consume();
    
    return 0;
}
