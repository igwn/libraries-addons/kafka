#! /bin/bash

# This file is used by docker-compose.yml as container warmup.

[[ $_ != $0 ]] && REALPATH=`dirname $(readlink -f ${BASH_SOURCE[0]})` || REALPATH=`dirname $(readlink -f $0)`
cd $REALPATH

# Build conda environment
ln -sf /mnt/local/ $HOME
MOTD=0 make env

source .bashrc
MOTD=0 make vendor

# [..]

echo "Container warmup complete. (read-only project directory: $PWD)"
exec "$@"