
# Version used during development
cmake_minimum_required(VERSION 3.20)
project(${PROJECT_NAME}/Tools)

# Include custom CMake modules and utilities
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/../cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

include(MessageColor)
include(FindPackageStandard)
include(ProjectArchitecture)
include(ParentCMakeOnly)

#
# Looking for script structure
message_title("Tools")

# Add make command to run tools
add_custom_target(tools COMMENT "Running tools")

FILE_SOURCES(SOURCES "${CMAKE_CURRENT_SOURCE_DIR}" ABSOLUTE)
DUMP_ARCHITECTURE(SOURCES RELATIVE_PATH "${CMAKE_SOURCE_DIR}")
GET_COMMON_PATH(SOURCEPATH_COMMON SOURCES)

#
# Looking for script files
foreach( SRC ${SOURCES} )

    get_filename_component(SOURCE ${SRC} NAME_WE)
    get_filename_component(SOURCEPATH ${SRC} PATH)

    file(RELATIVE_PATH SOURCEPATH_REL "${SOURCEPATH_COMMON}" "${SOURCEPATH}")
    if(NOT "${SOURCEPATH_REL}" STREQUAL "")
        list(APPEND SOURCEPATH_REL "/")
    endif()

    message_color("-- Preparing target C++ `./${SOURCEPATH_REL}${SOURCE}`" COLOR GREEN)

    add_executable(${SOURCE} ${SRC})
    target_link_package(${SOURCE} ${LIBRARY} REQUIRED)

    add_dependencies(tools ${SOURCE}) # Custom `make tools`
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${SOURCE}
        DESTINATION bin
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
        OPTIONAL
    )

endforeach()